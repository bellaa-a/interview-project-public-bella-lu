package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
import java.time.LocalDate;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class CreditCardController {

    // TODO: wire in CreditCard repository here (~1 line)
    private CreditCardRepository creditCardRepository;

    private UserRepository userRepository; // needed to determine if user exists

    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        // TODO: Create a credit card entity, and then associate that credit card with user with given userId
        //       Return 200 OK with the credit card id if the user exists and credit card is successfully associated with the user
        //       Return other appropriate response code for other exception cases
        //       Do not worry about validating the card number, assume card number could be any arbitrary format and length
        Optional<User> optionalUser = userRepository.findById(payload.getUserId()); // get associated user
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findByNumber(payload.getCardNumber());
        if (optionalUser.isPresent() && optionalCreditCard.isPresent()) {
            User user = optionalUser.get();
            CreditCard newCard = new CreditCard();
            newCard.setId(payload.getUserId());
            newCard.setIssuanceBank(payload.getCardIssuanceBank());
            newCard.setNumber(payload.getCardNumber());
            newCard.setOwner(user);
            user.addCreditCard(newCard);
            creditCardRepository.save(newCard);
            return ResponseEntity.ok(newCard.getId()); // added successfully
        } else {
            return ResponseEntity.badRequest().body(null); // user doens't exist or card already present
        }
    }

    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        // TODO: return a list of all credit card associated with the given userId, using CreditCardView class
        //       if the user has no credit card, return empty list, never return null
        List<CreditCard> cards = userRepository.findById(userId).get().getCreditCards(); // list of all credit cards
        List<CreditCardView> cardViews = new ArrayList<>();
        for (CreditCard card : cards) {
            CreditCardView cardView = new CreditCardView(card.getIssuanceBank(), card.getNumber());
            cardViews.add(cardView);
        }
        return ResponseEntity.ok(cardViews); // list of credit card views
    }

    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        // TODO: Given a credit card number, efficiently find whether there is a user associated with the credit card
        //       If so, return the user id in a 200 OK response. If no such user exists, return 400 Bad Request
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findByNumber(creditCardNumber);
        if (optionalCreditCard.isPresent()) {
            CreditCard card = optionalCreditCard.get();
            return ResponseEntity.ok(card.getOwner().getId());
        } else {
            return ResponseEntity.badRequest().body(null); // card doesn't exist
        }
    }

    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<String> updateBalanceHistory(@RequestBody UpdateBalancePayload[] payload) {
        //TODO: Given a list of transactions, update credit cards' balance history.
        //      1. For the balance history in the credit card
        //      2. If there are gaps between two balance dates, fill the empty date with the balance of the previous date
        //      3. Given the payload `payload`, calculate the balance different between the payload and the actual balance stored in the database
        //      4. If the different is not 0, update all the following budget with the difference
        //      For example: if today is 4/12, a credit card's balanceHistory is [{date: 4/12, balance: 110}, {date: 4/10, balance: 100}],
        //      Given a balance amount of {date: 4/11, amount: 110}, the new balanceHistory is
        //      [{date: 4/12, balance: 120}, {date: 4/11, balance: 110}, {date: 4/10, balance: 100}]
        //      Return 200 OK if update is done and successful, 400 Bad Request if the given card number
        //        is not associated with a card.

        // assuming all payloads are associated with a single card
        Optional<CreditCard> optionalCreditCard = creditCardRepository.findByNumber(payload[0].getCreditCardNumber());
        CreditCard card = new CreditCard();
        if (optionalCreditCard.isPresent()) {
            card = optionalCreditCard.get();
        } else { // card associated with payload doesn't exist
            return ResponseEntity.badRequest().body("Incorrect card number: " + payload[0].getCreditCardNumber());
        }

        for (UpdateBalancePayload transaction : payload) {
            LocalDate transactionDate = transaction.getBalanceDate();
            double transactionAmount = transaction.getBalanceAmount();

            if (card.dateExists(transactionDate)) { // date already associated with a balance
                card.updateBalanceHistory(transactionDate, transactionAmount);
            } else { // new balance needed to be added
                BalanceHistory newBalanceHistory = new BalanceHistory();
                newBalanceHistory.setId(card.getId());
                newBalanceHistory.setDate(transactionDate);
                newBalanceHistory.setBalance(transactionAmount);
                card.addBlanaceHistory(newBalanceHistory);
            }
            card.fillDateGaps(); // fill all gaps in balance history with previous balance
            creditCardRepository.save(card);
        }
        return ResponseEntity.ok("Update success");
    }
}
